//  This is a giant switch. If the sidebar element does not exist, create it and display it, else remove it from the DOM.
if (!document.getElementById("csSidebar")) {
  //Create an iFrame with the name/id csSidebar and src of www.codefriar.com
  //You can use any website not using extended http headers to prevent display
  //of the site in an Iframe. Specifically, google.com and facebook.com will
  //not allow display of their pages within an iframe. 
  csSidebar = document.createElement("iframe"); 
  csSidebar.name = csSidebar.id = "csSidebar";
  csSidebar.src = "http://www.codefriar.com";

  //Build out the CSS to handle the csSidebar.
  csSidebar_css = document.createElement("style");
  csSidebar_css.type = "text/css"; 
  sidebar_css = "#csSidebar{width:300px;height:100%;position:fixed;top:0;right:0;bottom:0;border:none}#csSidebar{border-left:solid #ddd 2px;z-index:10000;background:#fff}#csSidebarr{z-index:10000;display:none;opacity:0}";
  //inject the css into the css style element created on line 12.
  csSidebar_css.appendChild(document.createTextNode(sidebar_css));

  //Now for the important part:
  //This is executed in the context of the currently active tab
  //Append the css to the DOM
  document.body.appendChild(csSidebar_css);
  //Append the actual iframe to the DOM
  document.body.appendChild(csSidebar);
} else {
  //If the sidebar is currently visible, and the user has clicked
  //the sidebar button, remove the sidebar and css from the DOM to
  //toggle it "off"
  document.body.removeChild(csSidebar);
  document.body.removeChild(csSidebar);
}