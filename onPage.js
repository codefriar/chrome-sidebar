// The key to this entire project is ensuring that the chrome.tab.executescript call
// has a tab on which to attach/execute. Utilize this tiny bit of js code, in conjunction
// with the sidebar.js file stored separately for the sake of keeping things clean
// allows Chrome to know which tab to attach the sidebar to.
chrome.browserAction.onClicked.addListener(function(e) {
	chrome.tabs.executeScript(null, {file: "sidebar.js"});
	//the null in line 6 above forces chrome to choose the active tab!!
});	